import { ServiceDependency } from './service';
import { KeyValueConfiguration } from './configuration';

// Protocol queues
export const enum MycrosMessage {
  // Presence notifier
  PRESENCE = 'MYCROS_PRESENCE',
  S_CONFIG = 'MYCROS_S_CONFIG',
  INFO_REQ = 'MYCROS_INFO_REQ',
  INFO_ACK = 'MYCROS_INFO_ACK',
  DEP_SCAN = 'MYCROS_DEP_SCAN',
  DEP_RESP = 'MYCROS_DEP_RESP',
  PING_REQ = 'MYCROS_PING_REQ',
  PING_ACK = 'MYCROS_PING_ACK',
}

// Basic data for sending from service instance to manager
export interface MycrosServiceToManager {
  // Service name
  name: string;
  // Instance ID
  instance: string;
  // Timestamp
  date: Date;
}

// Basic data for sending from manager to service instance
export interface MycrosManagerToService {
  // Service name
  name?: string;
  // Instance ID
  instance?: string;
  // Timestamp
  date: Date;
}

// Presence broadcast packet (s->m)
export interface MycrosPresence extends MycrosServiceToManager {}

// Service configuration packet (m->s)
export interface MycrosConfig extends MycrosManagerToService {
  config: KeyValueConfiguration;
  deps: { [key: string]: string };
}

// Service information request packet (m->s)
export interface MycrosInfoReq extends MycrosManagerToService {}

// Service information request response (s->m)
export interface MycrosInfoAck extends MycrosServiceToManager {
  // Address of the service
  address: string;
  // Service image used
  image: string;
  // Image version used
  version: string;
  // Dependencies
  dependencies?: ServiceDependency[];
}

// Service status confirmation request packet (m->s)
export interface MycrosPingReq extends MycrosManagerToService {}

// Allowed service statuses
export enum MycrosStatus {
  'OK',
  'ERROR',
}

// Service status confirmation response packet (s->m)
export interface MycrosPingAck extends MycrosServiceToManager {
  // Service status
  status: MycrosStatus;
}
