import { Client, connect, NatsConnectionOptions } from 'ts-nats';
import { KeyValueConfiguration } from './configuration';

// Service dependency
export interface ServiceDependency {
  // Dependency name
  name: string;
  // Dependency image
  image: string;
  // Dependency accepted versions (semantic versioning)
  version: string;
  // Required?
  required?: boolean;
}

// Service configurations
export interface ServiceConfig {
  // Service image name
  image: string;
  // Service image version (semantic versioning)
  version: string;
  // Service name
  name?: string;
  // Service address
  address?: string;
  // Service dependencies
  dependencies?: ServiceDependency[];
  // Service options
  configuration?: KeyValueConfiguration;
  // Nats config or client
  nats?: NatsConnectionOptions | Client;
}
