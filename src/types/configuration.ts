// Input type specific data
// enum
export interface ServiceInputEnumData {
  // Option identifier
  id: string;
  // Option name
  name: string;
  // Option description
  description: string;
}

// Configuration input types
export enum ServiceOptionEnum {
  'boolean',
  'string',
  'number',
  'enum',
}

// Represents an input in a configuration
export interface ConfigurationInput {
  // Option name
  name: string;
  // Option description
  description: string;
  // Option type
  type: ServiceOptionEnum;
  // Option value
  value?: string;
  // Extra data for option
  optionData?: ServiceInputEnumData[];
}

// Represents a configuration
export interface KeyValueConfiguration {
  // Configuration name
  name?: string;
  // Options
  options: { [key: string]: ConfigurationInput };
}
