import {
  Client,
  connect,
  Msg,
  MsgCallback,
  NatsError,
  Subscription,
  NatsConnectionOptions,
} from 'ts-nats';
import { v4 as uuid } from 'uuid';

import {
  MycrosConfig,
  MycrosInfoAck,
  MycrosInfoReq,
  MycrosManagerToService,
  MycrosMessage,
  MycrosPingAck,
  MycrosPingReq,
  MycrosPresence,
  MycrosStatus,
} from './types/protocol';
import { ServiceConfig, ServiceDependency } from './types/service';
import { KeyValueConfiguration } from './types/configuration';

export class ServiceHandler {
  // Service name
  name: string;
  // Instance
  instance: string;
  // Image
  image: string;
  // Version
  version: string;
  // Address
  address: string;
  // Dependencies
  dependencies?: ServiceDependency[];
  // Options

  // Service's nats client
  nats!: Client;

  // Subscriptions
  // Configuration packet
  configSub!: Subscription;
  // Service Info Request packet
  infoReqSub!: Subscription;
  // Service Status Request packet
  statusReqSub!: Subscription;

  // Runtime properties
  // Service config
  config!: KeyValueConfiguration;
  // Dependencies
  deps: { [key: string]: string } = {};
  // Service status
  status: MycrosStatus = MycrosStatus.OK;

  // Messages a dependency
  async message(service: string, data: {}, cb: MsgCallback) {
    const inbox = this.nats.createInbox();

    await this.nats.subscribe(inbox, cb, { max: 1 });

    this.nats.publish(this.deps[service], data, inbox);
  }

  // Get option
  async option(option: string) {
    return this.config.options[option] || this.config.options[option].value;
  }

  constructor(config: ServiceConfig) {
    this.instance = uuid();

    // Setup basic configs
    this.name = process.env.SERVICE_NAME || config.name || config.image;
    this.image = config.image;
    this.version = config.version;
    this.address = process.env.SERVICE_ADDR || config.address || '';
    this.dependencies = config.dependencies;
  }

  async asyncInit(config: ServiceConfig) {
    // Default nats config
    let defaultNats: Client | NatsConnectionOptions | null = null;

    if (process.env.NATS_SERVERS) {
      const defaultServers = process.env.NATS_SERVERS.split(',');

      defaultNats = {
        servers: defaultServers,
      };
    }

    const nats: Client | NatsConnectionOptions | null =
      config.nats || defaultNats;

    if (!nats) return false;

    // Try connecting to nats
    let client: Client;
    if (nats instanceof Client) {
      client = nats;
    } else {
      console.log(`Connecting to nats`);
      try {
        client = await connect(nats);
      } catch (ex) {
        console.error(ex);
        return;
      }
      console.log(`Connected to nats!`);
    }

    this.nats = client;

    // Setup protocol subscriptions
    console.log(`Setting up subscriptions`);
    try {
      this.configSub = await this.nats.subscribe(
        MycrosMessage.S_CONFIG,
        this.handle
      );
      this.infoReqSub = await this.nats.subscribe(
        MycrosMessage.INFO_REQ,
        this.handle
      );
      this.statusReqSub = await this.nats.subscribe(
        MycrosMessage.PING_REQ,
        this.handle
      );
    } catch (ex) {
      console.error(ex);
      return;
    }
    console.log(`Set up subscriptions!`);

    // Initial exchange
    const presence: MycrosPresence = {
      name: this.name,
      instance: this.instance,
      date: new Date(),
    };

    console.log('Sending PRESENCE broadcast');
    this.nats.publish(MycrosMessage.PRESENCE, [presence]);
    console.log('Sent PRESENCE broadcast!');

    return true;
  }

  static async create(config: ServiceConfig) {
    console.log(`Initializing service...`);

    const handler = new ServiceHandler(config);

    if (!(await handler.asyncInit(config))) {
      console.error('Failed to initialize!');
      return null;
    }

    console.log(`Successfully initialized service!`);
    return handler;
  }

  // Message handling methods
  handle(err: NatsError | null, msg: Msg, subject?: MycrosMessage) {
    const data: MycrosManagerToService = msg.data;

    // Filter destination
    if (
      (!data.name || data.name === this.name) &&
      (!data.instance || data.instance === this.instance)
    ) {
      const subj = subject || msg.subject;
      // Forwards to correct handler
      if (subj === MycrosMessage.S_CONFIG) {
        this.configHandler(msg.data, msg);
      } else if (subj === MycrosMessage.INFO_REQ) {
        this.infoReqHandler(msg.data, msg);
      } else if (subj === MycrosMessage.PING_REQ) {
        this.statusReqHandler(msg.data, msg);
      }
    }
  }

  configHandler(data: MycrosConfig, msg: Msg) {
    this.config = data.config;
    this.deps = data.deps;
  }

  infoReqHandler(data: MycrosInfoReq, msg: Msg) {
    const reply: MycrosInfoAck = {
      name: this.name,
      instance: this.instance,
      date: new Date(),

      image: this.image,
      version: this.version,
      address: this.address,
      dependencies: this.dependencies,
    };

    if (msg.reply) {
      this.nats.publish(msg.reply, [reply]);
    } else {
      this.nats.publish(MycrosMessage.INFO_ACK, [reply]);
    }
  }

  statusReqHandler(data: MycrosPingReq, msg: Msg) {
    const reply: MycrosPingAck = {
      name: this.name,
      instance: this.instance,
      date: new Date(),

      status: this.status,
    };

    if (msg.reply) {
      this.nats.publish(msg.reply, [reply]);
    } else {
      this.nats.publish(MycrosMessage.PING_ACK, [reply]);
    }
  }
}
